const config = require('config');
const express = require('express');
const app = express();

if(!config.get('dolo_jwtPrivateKey') || !config.get('nodemailer_key')){
    throw new Error('Fatal error - jwtPrivateKey is not defined - check env variables');
}

require('./startup/logging')();
require('express-async-errors');
require('./startup/routes')(app);
require('./startup/prod')(app);
require('./startup/db')();
require('./startup/updateScores')();
require('./startup/clearTemp')();

app.use(function (error, req, res, next) {
    if (error instanceof SyntaxError) {
      return res.status(400).send("Syntax error");
    } else {
      next();
    }
});


app.listen(3900);