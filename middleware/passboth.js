const jwt = require('jsonwebtoken');
const config = require('config');

module.exports = (req, res, next) =>{
    let valid = req.header('x-user');
    try {
        const decoded = jwt.verify(valid, config.get('dolo_jwtPrivateKey'));
        if(req.body.user){
            req.body.user = decoded._id;
        }
        else {
            req.headers['x-user'] = decoded._id;
        }
        next();
    }
    catch (error){
        const token = req.header('x-auth-token');

        try {
            const decoded2 = jwt.verify(token, config.get('dolo_jwtPrivateKey'));
            if (!decoded2.login) throw new Error("Token used for password resets can't be used for login");
            req.user = decoded2;
            next();
        }
        catch (ex) {
            console.log(ex);
            return res.status(400).json({ msg: "Invalid token" });
        }
    }
}