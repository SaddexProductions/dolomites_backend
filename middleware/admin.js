const jwt = require('jsonwebtoken');
const config = require('config');

module.exports = function (req, doc) {
    const token = req.header('x-auth-token');
    try {
        const decoded = jwt.verify(token, config.get('dolo_jwtPrivateKey'));
        if (doc._id != decoded._id) {
            return false;
        }
        else {
            return true;
        }
    }
    catch(ex){
        return false;
    }
}