const jwt = require('jsonwebtoken');
const config = require('config');

module.exports = (req, _, next) =>{
    let valid;
    if(req.body.user){
        valid = req.body.user;
    }
    else {
        valid = req.header('x-user');
    }
    try {
        const decoded = jwt.verify(valid, config.get('dolo_jwtPrivateKey'));
        if(req.body.user){
            req.body.user = decoded._id;
        }
        else {
            req.headers['x-user'] = decoded._id;
        }
        next();
    }
    catch (ex){
        
    }
}