const mongoose = require('mongoose');
const { Ip, validateIP } = require('../models/ips');
const config = require('config');
const jwt = require('jsonwebtoken');

module.exports = function (req) {
    return new Promise(async (resolve, reject) => {
        const ip = req.header('x-real-ip');
        const no = req.header('x-no');

        console.log(ip.length);

        let id = req.header('x-user');
        if (id == "undefined") {
            id = false
        }
        if (!no) {
            if (id) {
                id = jwt.verify(id, config.get('dolo_jwtPrivateKey'));
                id = id._id;
                const { error } = validateIP({ ip: ip, id: id });
                if (error) return reject({ msg: error.details[0].message });
            }
            else {
                const { error } = validateIP({ ip: ip });
                if (error) return reject({ msg: error.details[0].message });
            }

            const nIp = ip.replace(/:|\./g, "_");

            if (id) {
                try {
                    let found = await Ip.findById(id);
                    if (!found) {
                        return reject({ msg: "Invalid id" })
                    };
                    return resolve(found.generateAuthToken());
                }
                catch (ex) {
                    return reject({ msg: "Internal server error" });
                }
            }
            else {
                let found = await Ip.findOne({ ip: nIp });
                if (!found) {
                    let ip = new Ip({ ip: nIp, reviews: [] });
                    found = await ip.save();
                    return resolve(ip.generateAuthToken());
                }
                return resolve(found.generateAuthToken());
            }
        }
        else {
            return resolve(false);
        }
    });
}