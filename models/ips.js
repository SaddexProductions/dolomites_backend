const mongoose = require('mongoose');
const Joi = require('joi');
Joi.ObjectId = require('joi-objectid')(Joi);
const config = require('config');
const jwt = require('jsonwebtoken');

const IpSchema = new mongoose.Schema({
  ip: {
      type: String,
      required: true,
      minlength: 10,
      maxlength: 40,
      unique: true
  }
},{collection: 'ips'});

IpSchema.methods.generateAuthToken = function(){
  const token = jwt.sign({
    _id: this._id
  }, config.get('dolo_jwtPrivateKey'));
  return token;
}

const Ip = mongoose.model('IP',IpSchema);

function validateIp(ip) {
    const schema = {
      ip: Joi.string().min(10).max(40).required(),
      id: Joi.ObjectId()  
    };
  
    return Joi.validate(ip, schema);
  }
  exports.Ip = Ip;
  exports.validateIP = validateIp;