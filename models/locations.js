const mongoose = require('mongoose');
const Joi = require('joi');
const ObjectId = mongoose.Schema.Types.ObjectId;

const locationSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true,
        minlength: 3,
        maxlength: 32,
        unique: true
    },
    desc: {
        type: String,
        required: true,
        minlength: 5,
        maxlength: 500,
    },
    alt: {
        type: Number,
        required: true,
        min: 0
    },
    hgw: Boolean,
    img: {
        type: String,
        minlength: 5,
        maxlength: 100,
        required: true
    },
    pos: {
        type: {
            type: String,
            required: true
        },
        coordinates: {
            type: [Number],
            required: true
        }
    },
    reviews: {
        type: [ObjectId],
        required: false
    },
    score: {
        type: Number,
        required: false
    }
}, { collection: 'locations' });

const Location = mongoose.model('Location', locationSchema);

function validateLocation(location) {
    const schema = {
        title: Joi.string().min(3).max(32).required(),
        desc: Joi.string().min(5).max(500).required(),
        alt: Joi.number().required(),
        hgw: Joi.boolean().required(),
        img: Joi.string().min(5).max(100).regex(/\.(jpg|JPG|gif|GIF|png|PNG|JPEG|jpeg)$/).required(),
        pos: Joi.object().keys({
            type: Joi.string().valid("Point").required(),
            coordinates: Joi.array().items(Joi.number().min(0).max(90).required(), Joi.number().min(0).max(180).required())
        })
    };

    return Joi.validate(location, schema);
}
exports.Location = Location;
exports.validate = validateLocation;