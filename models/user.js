const config = require('config');
const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const Joi = require('joi'); 

const userSchema = new mongoose.Schema({
  email: {
      type: String,
      required: true,
      minlength: 5,
      maxlength: 255,
      unique: true
  },
  password: {
      type: String,
      required: true,
      minlength: 5,
      maxlength: 1024,
  },
  validResetToken: String,
  passwordUpdatedOn: Date,
  isAdmin: Boolean
},{collection: 'users'});

userSchema.methods.generateAuthToken = function(login){
  const token = jwt.sign({
    _id: this._id,
    isAdmin: this.isAdmin,
    email: this.email,
    login: login,
    signedOn: new Date()
  }, config.get('dolo_jwtPrivateKey'));
  return token;
}

const User = mongoose.model('User',userSchema);

function validateUser(user) {
    const schema = {
      email: Joi.string().min(5).max(255).required().email(),
      password: Joi.string().min(5).max(255).required()
    };
  
    return Joi.validate(user, schema);
  }
  function validateNewPass(req){
    const schema = {
      password: Joi.string().min(5).max(255),
      newPassword: Joi.string().min(5).max(255).required(),
      repeatPassword: Joi.string().min(5).max(255).required()
    };

    return Joi.validate(req, schema);
  }
  function validateEmail(req){
    const schema = {
      email: Joi.string().min(5).max(255).required().email()
    };
    return Joi.validate(req, schema);
  }
  exports.User = User;
  exports.validate = validateUser;
  exports.validateNewPass = validateNewPass;
  exports.validateEmail = validateEmail;