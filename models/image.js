const mongoose = require('mongoose');
const Joi = require('joi');
Joi.ObjectId = require('joi-objectid')(Joi);
const ObjectId = mongoose.Schema.Types.ObjectId;

const ImageSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true,
    unique: true
  },
  type: {
    type: String,
    required: true
  },
  size: {
    type: Number,
    required: true
  },
  usedBy: [{
      name: {
        type: String,
        required: false,
      },
      _id: {
        type: ObjectId,
        required: false
      }
  }]
}, { collection: 'images' });

const Image = mongoose.model('Images', ImageSchema);

function validate(image) {
  const schema = {
    title: Joi.string().min(1).max(150).required(),
    type: Joi.string().min(3).max(4).required().regex(/^.*(jpg|JPG|gif|GIF|png|PNG|JPEG|jpeg)$/),
    size: Joi.number().required()
  };

  return Joi.validate(image, schema);
}
exports.Image = Image;
exports.validate = validate;