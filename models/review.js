const mongoose = require('mongoose');
const Joi = require('joi');
Joi.ObjectId = require('joi-objectid')(Joi);
const ObjectId = mongoose.Schema.Types.ObjectId;

const ReviewSchema = new mongoose.Schema({
  score: {
    type: Number,
    required: true
  },
  user: {
    type: ObjectId,
    required: true
  },
  location: {
    title: {
      type: String,
      required: true
    },
    _id: {
      type: ObjectId,
      required: true
    }
  }
}, { collection: 'reviews' });

const Review = mongoose.model('Reviews', ReviewSchema);

function validate(review) {
  const schema = {
    score: Joi.number().min(1).max(5).required(),
    user: Joi.ObjectId().required(),
    location: Joi.object().keys({
      title: Joi.string().required(),
      _id: Joi.ObjectId().required()
    })
  };

  return Joi.validate(review, schema);
}
exports.Review = Review;
exports.validate = validate;