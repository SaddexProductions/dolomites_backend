const _ = require('lodash');
const { User, validate, validateNewPass, validateEmail } = require('../models/user');
const mongoose = require('mongoose');
const auth = require('../middleware/auth');
const admin = require('../middleware/admin');
const nodemailer = require('nodemailer');
const config = require('config');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const express = require('express');
const hb = require('handlebars');
const fs = require('fs');
const { promisify } = require('util')

const readFileAsync = promisify(fs.readFile);
const writeFileAsync = promisify(fs.writeFile);

const router = express.Router();

router.post('/resetpassword', async (req, res) => {
    const { error } = validateEmail(req.body);
    if (error) return res.status(400).json({ msg: error.details[0].message });

    let user = await User.findOne({ email: req.body.email.toLowerCase() });
    if (!user) return res.status(404).json({ msg: "A user with this email was not found." });
    
    const token = user.generateAuthToken(false);
    try {
        user.validResetToken = token;
        await user.save();
        let transporter = nodemailer.createTransport({
            host: 'smtp.sendgrid.net',
            port: 465,
            secure: true, // true for 465, false for other ports
            auth: {
                user: "apikey",
                pass: config.get('nodemailer_key')
            }
        });
        const html = await readFileAsync('./resources/template.html', {encoding: 'utf-8'});
        let template = hb.compile(html);
        let replacements = {
            url: `https://dev.saddexproductions.com/api/users/resetpassword?token=${token}&email=${req.body.email.toLowerCase()}`
        };
        let htmlSend = template(replacements);
        const mailOptions = {
            from: "noreply@saddexproductions.com", // sender address
            to: req.body.email.toLowerCase(), // list of receivers
            subject: "Reset password", // Subject line
            html: htmlSend
        }
        transporter.sendMail(mailOptions, (error, info) => {
            if(err){
                throw(err);
            }
        });

        res.status(200).json({ msg: `A password reset email was sent to the adress ${user.email}` });
    }
    catch (ex) {
        return res.status(500).json({ msg: "Internal server error" });
    }
});
router.put('/resetpassword', async (req,res) =>{
    try {
        const decrypted = jwt.verify(req.header('x-reset-token'), config.get('dolo_jwtPrivateKey'));
        if(decrypted.login) return res.status(403).json({msg: "This token can't be used for that operation"});

        const user = await User.findOne({email: decrypted.email});
        if(!user) return res.status(404).json({msg: "A user with this email was not found."});

        const { error } = validateNewPass(req.body);
        if (error) return res.status(400).json({ msg: error });

        if (req.body.newPassword != req.body.repeatPassword) return res.status(400).json({ msg: "The passwords do not match" });

        const salt = await bcrypt.genSalt(10);
        user.password = await bcrypt.hash(req.body.newPassword, salt);
        user.validResetToken = "";
        user.passwordUpdatedOn = new Date();
        await user.save();

        res.status(200).json({msg: "Password updated successfully. Redirecting..."});
    }
    catch(ex){
        return res.status(400).json({msg: "Invalid token"});
    }

});
/* router.post('/', async (req, res) =>{

    const { error } = validate(req.body);
    if(error) return res.status(400).json({message: error.details[0].message});

    //loookup user in db

    let user = await User.findOne({ email: req.body.email.toLowerCase()});
    if(user) return res.status(400).json({message: "User already registered"});

    user = new User(
        _.pick(req.body, ['email', 'password'])
    );
    const salt = await bcrypt.genSalt(10);
    user.password = await bcrypt.hash(user.password,salt);
    await user.save();

    _.pick(user, ['_id','email'])

    res.status(200).json(user);
}); */

router.put('/:id', auth, async (req, res) => {

    const { error } = validateNewPass(req.body);
    if (error) return res.status(400).json({ msg: error.details[0].message });

    let user = await User.findById(req.params.id);
    if (!user) return res.status(404).json({ msg: "User not found" });

    let authorized = admin(req, user);
    if (!authorized) return res.status(403).json({ msg: "You're not authorized to exercise that operation." });

    if(!req.body.password) return res.status(401).json({msg: "Please enter the old password"});

    const validPassword = await bcrypt.compare(req.body.password, user.password);
    if (!validPassword) return res.status(400).json({ msg: 'Please enter the current password again, correctly' });

    const isSame = await bcrypt.compare(req.body.newPassword, user.password);
    if(isSame) return res.status(400).json({ msg: "The new password can't be the same as the old password" });

    if (req.body.newPassword != req.body.repeatPassword) return res.status(400).json({ msg: "The passwords do not match" });

    const salt = await bcrypt.genSalt(10);
    user.password = await bcrypt.hash(req.body.newPassword, salt);
    user.passwordUpdatedOn = new Date();
    const token = user.generateAuthToken(true);
    await user.save();

    res.status(200).json({ msg: "Updated password successfully", data: _.pick(user, ['_id', 'email']), token: token });

});
router.get('/resetpassword',async (req,res) => {
    const user = await User.findOne({validResetToken: req.query.token});
    if(!user) return res.status(400).send("<script type='text/javascript'>(function(){window.location.href = '../../../login';})()</script>");

    try {
        const decrypted = jwt.verify(req.query.token, config.get('dolo_jwtPrivateKey'));
        if(decrypted.email =! req.query.email || decrypted.login) return res.status(403).send("<script type='text/javascript'>(function(){window.location.href = '../../../login';})()</script>");
    }
    catch(ex){
        return res.status(400).json({msg: "Invalid token"});
    }
    const token = user.generateAuthToken(false);

    res.status(200).send("<script type='text/javascript'>(function(){var d = new Date();d.setTime(d.getTime() + (60 * 60 * 1000));var expires = 'expires=' + d.toUTCString();"+
    "document.cookie = 'reset = " + token + ";' + expires + ';path=/';;window.location.href = '../../../resetpassword';})()</script>");
});

module.exports = router;