const auth = require('../middleware/auth');
const mongoose = require('mongoose');
const _ = require('lodash');
const ipReg = require('../middleware/ipReg');
const express = require('express');
const router = express.Router();
const { Image } = require('../models/image');
const { Location, validate } = require('../models/locations');
const { Review } = require('../models/review');
const { Ip } = require('../models/ips');

router.get('/', async (req, res) => {
    let title = req.query.title;
    let desc = req.query.desc;
    let minHeight = req.query.minHeight;
    let maxHeight = req.query.maxHeight;
    let maxScore = req.query.maxScore;
    let minScore = req.query.minScore;
    let alt;
    let score;
    let limit = req.query.limit;
    let imgName = req.query.imgName;
    if (maxHeight && minHeight) {
        alt = {
            $gte: minHeight,
            $lte: maxHeight
        }
    }
    if (maxScore && minScore) {
        score = {
            $gte: minScore,
            $lte: maxScore
        };
    }
    if (title) title = { $regex: new RegExp(req.query.title, "i") };
    if (desc) desc = { $regex: new RegExp(req.query.desc, "i") };
    if (imgName) imgName = { $regex: new RegExp(req.query.imgName, "i") };
    let filters = {
        title: title,
        desc: desc,
        alt: alt,
        img: imgName,
        score: score
    }
    let propNames = Object.getOwnPropertyNames(filters);
    for (var i = 0; i < propNames.length; i++) {
        var propName = propNames[i];
        if (filters[propName] === null || filters[propName] === undefined) {
            delete filters[propName];
        }
    }
    if (req.query.hgw == "false" || req.query.hgw == "true") {
        filters.hgw = req.query.hgw;
    }
    if (req.query.la && req.query.lo && req.query.range) {
        filters.pos = {
            $geoWithin: {
                $centerSphere: [[req.query.la, req.query.lo], ((req.query.range) / 6378.1)]
            }
        }
    }
    let filters2 = JSON.parse(JSON.stringify(filters));
    filters2.title = filters.title;
    filters2.desc = filters.desc;
    filters2.score = { $exists: false };
    if (!limit) {
        limit = 0;
    }
    else {
        limit = parseInt(limit);
    }
    let locations;
    try {
        locations = await Location.find({ $or: [filters, filters2] }, { reviews: 0, __v: 0 }).sort({ title: 1 }).limit(limit);
        if(!locations || locations.length == 0) return res.status(404).json({msg: "No locations found!"});
    }
    catch (error) {
        return res.status(400).json({ msg: "Bad request" });
    }
    let id;
    try {
        id = await ipReg(req);
        if (!id) return res.status(200).json({ data: locations });
    }
    catch (ex) {
        return res.status(400).json(ex);
    }

    res.status(200).json({ data: locations, id: id });
});

router.get('/:id', async (req, res) => {

    try {
        const result = await Location.findById(req.params.id, { reviews: 0 });
        if (!result) {
            return res.status(404).json({ msg: "Not found" });
        }
        res.status(200).json(result);
    }
    catch (ex) {
        return res.status(400).json({ msg: "Bad request" });
    }
});
router.post('/', auth, async (req, res) => {
    const { error } = validate(req.body);
    if (error) return res.status(400).json({ msg: error.details[0].message });

    let location = await Location.findOne({ title: req.body.title.toLowerCase() });
    if (location) return res.status(400).json({ "msg": "Location already exists!" });

    let image = await Image.findOne({ title: (req.body.img).split('.')[0] });
    if (!image) return res.status(404).json({ msg: "Attached image not found!" });

    try {
        location = new Location(req.body);
        await location.save();

        const obj = {
            _id: mongoose.Types.ObjectId(location._id),
            name: location.title
        };
        if (!image.usedBy) {
            image.usedBy = [obj];
        }

        else {
            image.usedBy.push(obj);
        }
        await image.save();

        res.status(200).json(_.pick(location, ['pos', '_id', 'title', 'desc', 'alt', 'hgw', 'img', 'score']));
    }
    catch(ex){
        return res.status(500).json({msg: "Internal server error"});
    }
});

router.put('/:id', auth, async (req, res) => {
    const { error } = validate(req.body);
    if (error) return res.status(400).json({ msg: error.details[0].message });

    try {
        const old = await Location.findById(req.params.id);
        const location = await Location.findByIdAndUpdate(req.params.id, req.body, { new: true });

        let image = await Image.findOne({ title: (req.body.img).split('.')[0] });
        if (!image) return res.status(404).json({ msg: "Attached image not found!" });

        if (!image.usedBy) {
            image.usedBy = [{
                _id: mongoose.Types.ObjectId(location._id),
                name: location.title
            }];
            await image.save();
        }

        else {
            let found = false;
            for (let i = 0; i < image.usedBy.length; i++) {
                if (image.usedBy[i]._id == (location._id).toString()) {
                    found = true;
                    i = image.usedBy.length;
                }
            }
            if (!found) {
                image.usedBy.push({
                    _id: mongoose.Types.ObjectId(location._id),
                    name: location.title
                });
                await image.save();
            }
        }
        if (old.img != location.img) {
            await Image.findOneAndUpdate({ title: (old.img).split('.')[0], type: (old.img).split('.')[1]}, { $pull: { usedBy: { _id: location._id } } });
        }
        if(old.img == location.img && (old.title).toString() != (location.title).toString() ){
            await Image.findOneAndUpdate(
                {title: (location.img).split('.')[0],type: (location.img).split('.')[1], "usedBy._id":location._id},{$set: {"usedBy.$.name": location.title}}
                );
        }

        res.status(200).json(_.pick(location, ['pos', '_id', 'title', 'desc', 'alt', 'hgw', 'img', 'score']));
    }
    catch (ex) {
        console.log(ex);
        return res.status(400).json({ msg: "Bad request" });
    }
});

router.delete('/:id', auth, async (req, res) => {
    try {
        const result = await Location.findByIdAndDelete(req.params.id, { reviews: 0 });
        if (!result) return res.status(404).json({ msg: "Item not found" });
        const reviews = await Review.deleteMany({ location: req.params.id });
        for (let i = 0; i < reviews.length; i++) {
            await Ip.findByIdAndUpdate(reviews[i].user, { $pull: { reviews: { $in: review._id } } });
        }
        await Image.findOneAndUpdate({ title: (result.img).split('.')[0], type: (result.img).split('.')[1]}, { $pull: { usedBy: { _id: result._id } } });
        res.status(200).json(result);
    }
    catch (ex) {
        return res.status(500).json({ msg: "Internal server error" });
    }
});

module.exports = router;