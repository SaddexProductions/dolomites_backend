const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const _ = require('lodash');
const fs = require('fs');
const auth = require('../middleware/auth');
const fileUpload = require('express-fileupload');
const { Image, validate } = require('../models/image');
const { Location } = require('../models/locations');
router.use(fileUpload({
    useTempFiles: true,
    limits: {
        fileSize: 4000000 //4mb
    },
    abortOnLimit: true,
    tempFileDir: '/tmp/'
}));

router.post('/', auth, async (req, res) => {
    if (!req.files || Object.keys(req.files).length === 0) {
        return res.status(400).send('No files were uploaded.');
    }
    const file = req.files.file;
    const name = file.name;
    let altN = req.query.title || name.split('.')[0];
    let temp = req.query.temp;
    if (temp) {
        temp = "temp/";
    }
    else {
        temp = "";
    }
    const patt = /^.*\.(jpg|JPG|gif|GIF|png|PNG|JPEG|jpeg)$/;
    if (!patt.test(name)) {
        return res.status(400).json({ "msg": "Wrong file format" });
    }

    const ext = name.split('.');
    if (ext.length > 2) return res.status(400).json({ msg: "More than one file extension isn't allowed" });

    const dbFile = {
        title: altN,
        type: name.split('.')[1],
        size: file.size
    }

    const { error } = validate(dbFile);
    if (error) return res.status(400).json({ msg: error.details[0].message });

    const exists = await Image.findOne({ title: altN.toLowerCase() });
    if (exists && !temp) return res.status(400).json({ msg: "An image with that name already exists within the database" });

    let result;
    if (!temp) {
        const dbimage = new Image(dbFile);

        result;
        try {
            result = await dbimage.save();
        }
        catch (ex) {
            return res.status(500).json("Internal Server Error");
        }
    }

    file.mv(`../dev/img/${temp}${altN}.${name.split('.')[1]}`, async function (err) {
        if (err) {
            if (!temp) {
                await Image.findByIdAndDelete(result._id);
            }
            return res.status(500).json(err);
        }
        else {
            if (!temp) return res.status(200).json(_.pick(result, ['_id', 'title', 'type', 'size', 'usedBy']));

            res.status(200).json({ fileStats: dbFile, temp: true });
        }
    });
});

router.get('/', auth, async (req, res) => {
    let filter = {};
    let title, ext;
    let l = parseInt(req.query.limit);
    let sS = parseInt(req.query.minSize);
    let mS = parseInt(req.query.maxSize);
    let usedBy = req.query.usedBy;
    if (usedBy) {
        usedBy = (usedBy.replace(/\s/g, '')).split(",");
        filter = {
            "usedBy.name": { $in: usedBy }
        }
    }
    if (req.query.title) {
        title = {
            $regex: new RegExp(req.query.title, "i")
        }
        filter.title = title;
    }
    if (req.query.ext) {
        ext = {
            $regex: new RegExp(req.query.ext, "i")
        }
        filter.type = ext;
    }
    if (sS && mS) {
        filter.size = {
            $gte: (sS * 1000),
            $lte: (mS * 1000)
        }
    }
    if (!l) l = 0;

    const results = await Image.find(filter, { __v: 0 }).sort({ title: 1 }).limit(l);
    if (!results || results.length == 0) return res.status(404).json({ msg: "No images found" });

    res.status(200).json(results);
});

router.delete('/:id', auth, async (req, res) => {
    const temp = req.query.temp;
    if (!temp) {
        try {
            const result = await Image.findByIdAndDelete(req.params.id);
            if (!result) return res.status(404).json({ msg: "The image doesn't exist" });

            await Location.findOneAndUpdate({ img: `${result.title}.${result.type}` }, { img: "placeholder.png" });
            fs.unlinkSync(`../dev/img/${result.title}.${result.type}`);
            res.status(200).json(result);
        }
        catch (ex) {
            return res.status(500).json({ msg: "Internal Server Error" });
        }
    }
    else {
        try {
            let str = req.params.id;
            str.replace("%2E", ".");
            fs.unlinkSync(`../dev/img/temp/${str}`);
            res.status(200).json({ msg: "Image discarded." });
        }
        catch (ex) {
            return res.status(404).json({ msg: "Image not found" });
        }
    }
});
router.put('/:id', auth, async (req, res) => {
    const tempName = req.body.temp;
    delete req.body.temp;
    let alreadyRenamed;

    const { error } = validate(req.body);
    if (error) return res.status(400).json(error.details[0].message);

    let oldImage = await Image.findById(req.params.id);
    if (!oldImage) return res.status(404).json({ msg: "No image with the submitted id was found" });

    try {
        const checkIfOtherWithSameTitle = await Image.findOne({ title: req.body.title.toLowerCase(), _id: { $ne: req.params.id } });
        if (checkIfOtherWithSameTitle) return res.status(400).json({ msg: "Image name must be unique" });
        try {
            const result = await Image.findByIdAndUpdate(req.params.id, req.body, { new: true });
            if (result.size != oldImage.size) {
                const oldPath = `../dev/img/temp/${tempName}`;
                const newPath = `../dev/img/${req.body.title}.${req.body.type}`;
                fs.unlinkSync(`../dev/img/${oldImage.title}.${oldImage.type}`);
                await renameImage(oldPath,newPath);
                alreadyRenamed = true;
            }
            if (oldImage.title !== result.title || oldImage.type !== result.type) {
                await Location.updateMany({ img: `${oldImage.title}.${oldImage.type}` }, { $set: { img: `${result.title}.${result.type}` } });
                const oldPath = `../dev/img/${oldImage.title}.${oldImage.type}`;
                const newPath = `../dev/img/${req.body.title}.${req.body.type}`;
                if(!alreadyRenamed) await renameImage(oldPath,newPath);
            }
            return res.status(200).json(_.pick(result, ['_id', 'title', 'type', 'size', 'usedBy']));
        }
        catch (ex) {
            return res.status(500).json({ msg: "Internal Server Error" });
        }
    }
    catch (ex) {
        return res.status(500).json({ msg: "Internal Server Error" });
    }
});
async function renameImage(oldPath, newPath) {
    return new Promise((resolve, reject) => {
        fs.rename(oldPath, newPath, async function (err) {

            if (err) {
                try {
                    await Image.findByIdAndUpdate(req.params.id, oldImage);
                    return reject(err);
                }
                catch (ex) {
                    return reject(ex);
                }
            }
            resolve();
        });
    });
}


module.exports = router;