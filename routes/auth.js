const auth = require('../middleware/auth');
const bcrypt = require('bcrypt');
const _ = require('lodash');
const Joi = require('joi');
const {User} = require('../models/user');
const mongoose = require('mongoose');
const express = require('express');
const router = express.Router();

router.get('/', auth, async (req, res) => {
    const user = req.header('x-user-id');
    res.status(200).json({msg: "Redirecting...", _id: user._id});
});

router.post('/', async (req, res) => {
    const { error } = validate(req.body); 
    if (error) return res.json({msg: error.details[0].message});

    let user = await User.findOne({ email: req.body.email.toLowerCase() });
    if(!user) return res.json({success: false, msg: 'Invalid email or password.'});

    const validPassword = await bcrypt.compare(req.body.password, user.password);
    if(!validPassword) return res.json({success: false, msg: 'Invalid email or password.'});

    const token = user.generateAuthToken(true);

    res.status(200).json({success: true, jwt: token});
});
function validate(req) {
    const schema = {
      email: Joi.string().min(5).max(255).required().email(),
      password: Joi.string().min(5).max(255).required()
    };
  
    return Joi.validate(req, schema);
}

module.exports = router;