const mongoose = require('mongoose');
const auth = require('../middleware/auth');
const { Ip } = require('../models/ips');
const Joi = require('joi');
Joi.ObjectId = require('joi-objectid')(Joi);
const express = require('express');
const router = express.Router();

router.get('/', auth, async (req, res) => {
    let id = req.query.id;
    let filter = {};
    if (id != undefined && id != "") {
        filter = { _id: id };
        const { error } = Joi.validate(filter, { _id: Joi.ObjectId() });
        if (error) return res.status(400).json({ msg: error.details[0].message });
    }

    const results = await Ip.find(filter, { ip: 0 }).limit(10);
    if (!results) return res.status(404).json({ msg: "No results found" });

    res.status(200).json(results);
});

module.exports = router;