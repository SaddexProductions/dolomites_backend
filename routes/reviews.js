const express = require('express');
const router = express.Router();
const ipVal = require('../middleware/validIP');
const passBoth = require('../middleware/passboth');
const mongoose = require('mongoose');
const math = require('mathjs');
const auth = require('../middleware/auth');
const calc = require('../misc_modules/calc');
const { Ip } = require('../models/ips');
const { Review, validate } = require("../models/review");
const { Location } = require("../models/locations");

router.post('/', ipVal, async (req, res) => {

    const { error } = validate(req.body);
    if (error) return res.status(400).json({ msg: error.details[0].message });

    const id = req.body.user;
    let user;
    let location;

    user = await Ip.findById(id);
    if (!user) return res.status(404).json({ msg: "Invalid id" });

    location = await Location.findById(req.body.location);
    if (!location) return res.status(404).json({ msg: "Location not found!" });

    //transaction should start here

    let review = await Review.findOneAndUpdate({ user: id, "location._id": req.body.location._id },
        {
            user: mongoose.Types.ObjectId(id),
            location: {
                title: req.body.location.title,
                _id: mongoose.Types.ObjectId(req.body.location._id),
            },
            score: req.body.score
        }, { upsert: true, new: true });

    if (!location.reviews) location.reviews = [];
    location.reviews.push(mongoose.Types.ObjectId(review._id));
    await location.save();

    //transaction should end here

    res.status(200).json({ msg: "Rating saved successfully", score: review.score });
});

router.get('/:id', ipVal, async (req, res) => {
    const review = await Review.findOne({ "location._id": req.params.id, user: req.header('x-user') });
    if (!review) return res.status(404).json({ msg: "Rating not found!" });

    res.status(200).json(review);
});

router.get('/location/:id', auth, async (req, res) => {
    let minScore = req.query.minScore;
    let maxScore = req.query.maxScore;
    let user = req.query.user;
    let findObj = { "location._id": req.params.id };
    if (maxScore && minScore) {
        findObj.score = { $gte: minScore, $lte: maxScore };
    }
    let extra = "";

    const location = await Location.findById(req.params.id);
    if (!location) return res.status(404).json({ msg: "Location not found!" });

    if (user) {
        findObj.user = user;
        extra = " under this user"
    }
    const result = await Review.find(findObj);
    if (!result || result.length == 0) return res.status(404).json({ msg: `No reviews found for this location ${extra}!` });

    res.status(200).json(result);
});

router.get('/user/:id', auth, async (req, res) => {
    let minScore = req.query.minScore;
    let maxScore = req.query.maxScore;
    let findObj = { user: req.params.id };
    if (maxScore && minScore) {
        findObj.score = { $gte: minScore, $lte: maxScore };
    }
    const result = await Review.find(findObj, { __v: 0 });
    if (!result) return res.status(404).json({ msg: "No reviews found for this user!" });

    res.status(200).json(result);
});

router.delete('/:id', passBoth, async (req, res) => {
    const result = await Review.findOneAndDelete({ "location._id": req.params.id, user: req.header('x-user') });
    if (!result) {
        return res.status(404).json({ msg: "Rating not found!" });
    }
    else {
        await Location.findByIdAndUpdate(req.params.id, { $pull: { reviews: { $in: result._id } } });

        res.status(200).json(result);
    }
});
router.get('/scores/:id', auth, async (req, res) => {
    try {
        let average = await calc(Review, req.params.id);
        average = (math.floor((average * 10))) / 10;
        try {
            await Location.findByIdAndUpdate(req.params.id, { $set: { score: parseFloat(average) } });
            res.status(200).json({ score: average });
        }
        catch (ex) {
            return res.status(500).json({ msg: ex });
        }
    }
    catch (ex) {
        return res.status(ex[0]).json({ msg: ex[1] });
    }
});

module.exports = router;