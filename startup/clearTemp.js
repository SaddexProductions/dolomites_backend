const fs = require('fs');
const uploadsDir = "../dev/img/temp/";
const rimraf = require('rimraf');
const path = require('path');

module.exports = function(){
    setInterval(function(){
        fs.readdir(uploadsDir, function(err, files) {
            files.forEach(function(file, index) {
              fs.stat(path.join(uploadsDir, file), function(err, stat) {
                var endTime, now;
                if (err) {
                  console.error(err);
                }
                now = new Date().getTime();
                endTime = new Date(stat.ctime).getTime() + 3600000;
                if (now > endTime) {
                  return rimraf(path.join(uploadsDir, file), function(err) {
                    if (err) {
                      console.error(err);
                    }
                    console.log('successfully deleted');
                  });
                }
              });
            });
          });
    },3600*1000);
}