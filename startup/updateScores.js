const mongoose = require('mongoose');
const calc = require('../misc_modules/calc');
const { Review } = require("../models/review");
const { Location } = require("../models/locations");
const math = require('mathjs');

module.exports = function(){
    let now = new Date();
    let millisTill10 = new Date(now.getFullYear(), now.getMonth(), now.getDate(), 10, 0, 0, 0) - now;
    if (millisTill10 < 0) {
        millisTill10 += 86400000; // it's after 10am, try 10am tomorrow.
    }
    setInterval(async function(){
        const locations = await Location.find();
        for(let i=0;i<locations.length;i++){
            try {
                let average = await calc(Review, locations[i]._id);
                average = (math.floor((average*10)))/10;
                try {
                    await Location.findByIdAndUpdate(locations[i]._id, {$set: {score: parseFloat(average)}});
                }
                catch(ex){
                    console.log(ex);
                }
            }
            catch(ex){
                console.log(ex);
            }
        }
    }, millisTill10);
}