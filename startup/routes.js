const auth = require('../routes/auth');
const users = require('../routes/users');
const reviews = require('../routes/reviews');
const locations = require('../routes/locations');
const images = require('../routes/images');
const ips = require('../routes/ips');
const bodyParser = require('body-parser');
const error = require('../middleware/errors');

module.exports = function(app){
    app.use(bodyParser.urlencoded({ extended: false }));
    app.use(bodyParser.json());
    app.use('/api/locations/',locations);
    app.use('/api/login',auth);
    app.use('/api/users',users);
    app.use('/api/reviews',reviews);
    app.use('/api/images',images);
    app.use('/api/ips',ips);
    app.use(error);
}