const mongoose = require('mongoose');

module.exports = function (Review, _id) {
    return new Promise(async (resolve, reject) => {
        try {
            const results = await Review.find({ "location._id": _id });
            if(!results) return reject([404, "No reviews found for this location"]);

            let sum = 0;
            for (let i = 0; i < results.length; i++) {
                sum += results[i].score;
            }
            let average = sum / results.length;
            if(!average) return reject([404,"No reviews found for this location"]);
            resolve(average);
        }
        catch(ex){
            return reject([500, ex]);
        }
    });
}